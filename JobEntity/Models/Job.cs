﻿using Microsoft.Azure.KeyVault.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace JobEntity.Models
{
    public class Job
    {
        [Key]
        public int JobId { get; set; }

        [Column(TypeName ="varchar(256)")]
        [Required(ErrorMessage ="Provide a job title")]
        [DisplayName("Job Title")]
        public string JobTitle { get; set; }

        [Column(TypeName = "varchar(1000)")]
        [Required(ErrorMessage ="Provide a job description")]
        [DisplayName("Job Description")]
        public string Description { get; set; }

        [Column(TypeName = "DateTime")]
        public DateTime CreatedAt { get; set; }

        [Column(TypeName = "DateTime")]
        [DisplayName("Expires At")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime ExpiresAt { get; set; }
    }
}
