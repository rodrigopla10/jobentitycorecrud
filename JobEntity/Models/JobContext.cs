﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JobEntity.Models
{
    public class JobContext:DbContext
    {
        public JobContext(DbContextOptions<JobContext> options):base(options)
        {

        }

        public DbSet<Job> Jobs { get; set; }
        public DbSet<JobSeeker> JobSeekers { get; set; }
    }
}
