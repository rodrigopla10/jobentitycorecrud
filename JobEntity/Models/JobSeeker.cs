﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace JobEntity.Models
{
    public class JobSeeker
    {
        [Key]
        public int SeekerId { get; set; }

        [ForeignKey("Job")]
        public int JobId { get; set; }

        [Column(TypeName = "varchar(256)")]
        [Required(ErrorMessage = "Provide first name")]
        [DisplayName("First Name")]
        public string Name { get; set; }

        [Column(TypeName = "varchar(256)")]
        [Required(ErrorMessage = "Provide last name")]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [Column(TypeName = "varchar(20)")]
        [DisplayName("Mobile number")]
        public string Mobile { get; set; }

        [Column(TypeName = "varchar(256)")]
        [DisplayName("Address")]
        [Required(ErrorMessage = "Provide an address")]
        public string Address { get; set; }

        [Column(TypeName = "varchar(28)")]
        [DisplayName("City")]
        [Required(ErrorMessage = "Provide a city")]
        public string City { get; set; }

        [Column(TypeName = "varchar(1)")]
        [DisplayName("Gender")]
        [Required(ErrorMessage = "Provide a your Gender")]
        public string Gender { get; set; }
        
        [Column(TypeName = "DateTime")]
        [DisplayName("Birth Date")]
        [Required(ErrorMessage = "Provide a your Birth Date")]
        public DateTime BirthDate { get; set; }

        [Column(TypeName = "varchar(100)")]
        [DisplayName("Degree")]
        [Required(ErrorMessage = "Provide degree achieved")]
        public string Degree { get; set; }

        [Column(TypeName = "int")]
        [DisplayName("Years of experience")]
        [Required(ErrorMessage = "Provide years of experience")]
        public string Experience { get; set; }

        [Column(TypeName = "varchar(320)")]
        [DisplayName("Email")]
        [Required(ErrorMessage = "Provide an email to be contacted")]
        public string Email { get; set; }

    }
}
