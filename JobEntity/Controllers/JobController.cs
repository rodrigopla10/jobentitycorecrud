﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using JobEntity.Models;
using JobEntity.Helpers;
using Microsoft.WindowsAzure.Storage;
using Microsoft.IdentityModel.Tokens;
using static JobEntity.Helpers.Helper;

namespace JobEntity.Controllers
{
    public class JobController : Controller
    {
        private readonly JobContext _context;
        private readonly IDataRepository<Job> _repo;

        public JobController(JobContext context, IDataRepository<Job> repo)
        {
            _context = context;
            _repo = repo;
        }

        // GET: Job
        public async Task<IActionResult> Index()
        {
            return View(await _context.Jobs.ToListAsync());
        }

       
        // GET: Job/Create
        [NoDirectAccessAttribute]
        public async Task<IActionResult> AddOrEdit(int id=0)
        {
            try
            {
                if (id == 0)
                    return View(new Job());
                else
                {
                    var jobModel = await _context.Jobs.FindAsync(id);

                    if (jobModel == null)
                    {
                        return NotFound();
                    }
                    return View(jobModel);
                }
            }
            catch (Exception e)
            {

                e.Message.ToString();
                return View(new Job());
            }
        }

        // POST: Job/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddOrEdit([Bind("JobId,JobTitle,Description,CreatedAt,ExpiresAt")] Job job)
        {
            try
            {
           
                if (ModelState.IsValid)
                {
                    if (job.JobId == 0)
                    {
                        job.CreatedAt = DateTime.Now;
                        _repo.Add(job);
                         await _repo.SaveAsync(job);
                    }
                    else
                    {
                        _repo.Update(job);
                        await _repo.SaveAsync(job);
                    }

                    //return RedirectToAction(nameof(Index));
                    return Json(new { isValid = true, html =Helper.RenderRazorViewToString(this,"_ViewAll",_context.Jobs.ToList()) });
                }

            }
            catch (Exception e)
            {
                e.Message.ToString();
            }
            return Json(new { isValid = true, html = Helper.RenderRazorViewToString(this, "AddOrEdit", job) });
        }

      
        // GET: Job/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
           var job=await _context.Jobs.FindAsync(id);

            try
            {
                _repo.Delete(job);
                 await _repo.SaveAsync(job);
            }
            catch (Exception e)
            {
                e.Message.ToString();
            }

            return Json(new {html = Helper.RenderRazorViewToString(this, "_ViewAll", _context.Jobs.ToList()) });
        }


    }
}
