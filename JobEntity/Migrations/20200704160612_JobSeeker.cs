﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace JobEntity.Migrations
{
    public partial class JobSeeker : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Jobs",
                type: "varchar(1000)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "varchar(1000)",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "JobSeekers",
                columns: table => new
                {
                    SeekerId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Address = table.Column<string>(type: "varchar(256)", nullable: false),
                    BirthDate = table.Column<DateTime>(type: "DateTime", nullable: false),
                    City = table.Column<string>(type: "varchar(28)", nullable: false),
                    Degree = table.Column<string>(type: "varchar(100)", nullable: false),
                    Email = table.Column<string>(type: "varchar(320)", nullable: false),
                    Experience = table.Column<string>(type: "int", nullable: false),
                    Gender = table.Column<string>(type: "varchar(1)", nullable: false),
                    JobId = table.Column<int>(nullable: false),
                    LastName = table.Column<string>(type: "varchar(256)", nullable: false),
                    Mobile = table.Column<string>(type: "varchar(20)", nullable: true),
                    Name = table.Column<string>(type: "varchar(256)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobSeekers", x => x.SeekerId);
                    table.ForeignKey(
                            name: "FK_JobSeekers_Job",
                            column: x => x.JobId,
                            principalTable: "Jobs",
                            principalColumn: "JobId",
                            onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JobSeekers");

         }
    }
}
